import React, {useEffect, useState} from "react";
import {TouchableOpacity, SafeAreaView, RefreshControl, Button, Dimensions, ScrollView, Image, StyleSheet, View, Text} from "react-native";
import { useSelector} from "react-redux";
import {getBookList} from '../../data/mutation';

const {width, height} = Dimensions.get('window');
const wait = (timeout) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
} 

const Latest = ({navigation}) => {
    const bookList = useSelector(state => state.bookList);
    const [refreshing, setRefreshing] = useState(false);
    
    const onRefresh = React.useCallback(() => {
        wait(2000).then(() => setRefreshing(false));
    }, [refreshing]);

    return (
        <SafeAreaView>
            <ScrollView refreshControl={
                <RefreshControl refreshing={refreshing}
                onRefresh={onRefresh} />
            }>
            {bookList && bookList.map((item, index) => {
                return(
                    <View style={style.container} key={index}>
                        <TouchableOpacity onPress = {()=> {navigation.navigate('Detail',{id:item.id})}}>
                            <View style={style.card} >
                                <Image style={style.image} source={{uri: item.cover_image}}/>
                                <Text style={style.textTitle}>{item.title}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            })}
            </ScrollView>
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    container : {
        flex : 1,
        flexDirection: 'row',
        width: width,
        height: 210,
    },
    card:{
        width: 117,
        height: 200,
        marginTop: 10,
        marginBottom: 20
    },
    textTitle:{
        fontWeight: "bold",
        color: "black",
        alignSelf: "center",
        
    },
    image:{
        width: 117,
        height: 167,
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        borderRadius: 10,
        marginRight: 20
    }
})

export default Latest;