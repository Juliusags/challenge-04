import React, {useEffect, useState} from "react";
import { useSelector } from "react-redux";
import {SafeAreaView,RefreshControl, ScrollView, Image, StyleSheet, View, Text} from "react-native";

const wait = (timeout) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
} 

const Recommended = () => {
    const bookList = useSelector(state => state.bookList);
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = React.useCallback(() => {
        wait(2000).then(() => setRefreshing(false));
    }, [refreshing]);

    return(
    <SafeAreaView>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} 
            refreshControl={
            <RefreshControl refreshing={refreshing}
             onRefresh={onRefresh} />
        }>
            {bookList && bookList.map((item, index) => {
                return(
                    <View key={index}>
                        <Image style={style.image} source={{uri: item.cover_image}}/>
                    </View>
                )})}
        </ScrollView>
    </SafeAreaView>
    )
}

const style = StyleSheet.create({
    image : {
        borderRadius: 10,
        width: 147,
        height: 210,
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10

    }
})

export default Recommended;