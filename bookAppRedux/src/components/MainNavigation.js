import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import Login from "./screens/Login";
import Register from "./screens/Register";
import Warning from "./modul/Warning";
import Home from "./screens/Home";
import Detail from "./screens/Detail";

const navigate = createNativeStackNavigator();

const MainNavigation = () => {
    return (
        <NavigationContainer>
        <navigate.Navigator initialRouteName="Login" headerMode="none">
            <navigate.Screen name="Home" component={Home} />
            <navigate.Screen name="Detail" component={Detail} />
            <navigate.Screen name="Login" component={Login} />
            <navigate.Screen name="Register" component={Register} />
            <navigate.Screen name="Warning" component={Warning} />
        </navigate.Navigator>
        </NavigationContainer>
    );
}

export default MainNavigation;