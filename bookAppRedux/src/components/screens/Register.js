import React, { useEffect } from "react";
import {SafeAreaView, View, Image, TextInput, Button, Text, StyleSheet, Dimensions} from "react-native";
import { useDispatch, useSelector, Provider } from "react-redux";
import { userRegister } from "../../data/mutation";
import store from "../../data/store";


const {width, height} = Dimensions.get('window');
const inp = width - (width/2)+10/100*width;

const Register = ({navigation}) => {
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const jwt = useSelector(state => state.jwt);
    const dispatch = useDispatch();

    useEffect(() => {
        jwt && navigation.navigate('Home');
    } , [jwt]);
    const onRegister = () => dispatch(userRegister({name, email, password}));
    const onLogin = () => navigation.navigate('Login');
    return (
        <Provider store={store}>
            <SafeAreaView style={style.container}>
                <View>
                    <Text style={style.textJargon}> Let's See The World </Text>
                    <TextInput style={style.input} value={name} onChangeText={setName} placeholder="Full Name"/>
                    <TextInput style={style.input} value={email} onChangeText={setEmail} placeholder="Email"/>
                    <TextInput style={style.input} value={password} onChangeText={setPassword} placeholder="Password"/>   
                    <Button onPress={onRegister} width="200" height="46" title="Register" color="black"/>
                    <Text>Already have an account ?</Text>
                    <Text style={style.intent} onPress={onLogin}>Login</Text>
                </View>
            </SafeAreaView>
        </Provider>
    )
}

const style = StyleSheet.create ({
    container: {
        flex: 1,
        width: width,
        height: height,
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: "center",
    },
    textJargon:{
        fontSize: 30,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    view:{
        flexDirection: 'column',
        marginBottom: 20
    },
    input : {
        alignSelf: 'center',
        borderRadius: 20,
        margin: 5,
        backgroundColor: 'white',
        width: inp,
        marginBottom: 20
    },
    image:{
        alignSelf: 'center'
    },
    intent:{
        color: 'black',
        fontWeight: 'bold'
    }
})

export default Register;