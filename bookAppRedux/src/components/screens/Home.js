import React, {useEffect} from "react";
import {Dimensions, View, StyleSheet, Text, SafeAreaView, Alert} from "react-native";
import Recommended from "../modul/Recommended";
import Latest from "../modul/Latest";
import { useDispatch, useSelector} from "react-redux";
import {getBookList} from "../../data/mutation";

const {width, height} = Dimensions.get("window");

const Home = ({navigation}) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getBookList({}));
    }, [])

    return(
        <SafeAreaView style={style.container}>
            <View style={style.header}>
                <Text style={style.titleText}>Recommended</Text>
                <Recommended/>
            </View>
            <View style={style.body}>
                <Text style={style.titleText}>Latest Upload</Text>
                <Latest navigation={navigation}/>
            </View>

        </SafeAreaView>
    );
}

const style = StyleSheet.create({
    container : {
        flex : 1,
        width: width,
        height: height,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20
    },
    header : {
        flex: 2,
        marginBottom: 20
    },
    body : {
        flex: 3
    },
    titleText: {
        color: "black",
        fontSize: 24,
        fontWeight: "bold",
    }
})

export default Home;