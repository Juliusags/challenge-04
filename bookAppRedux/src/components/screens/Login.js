import React, {useEffect, useState} from "react";
import {SafeAreaView, View, Image, TextInput, Button, Text, StyleSheet, Dimensions} from "react-native";
import Bg from '../../assets/bg.png'
import {useDispatch, useSelector} from 'react-redux';
import { userLogin } from "../../data/mutation";
import store from "../../data/store";

const {width, height} = Dimensions.get('window');
const inp = width - (width/2)+10/100*width;


const Login = ({navigation}) => {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const jwt = useSelector(state => state.jwt);
    const dispatch = useDispatch();

    useEffect(() => {
        jwt && navigation.navigate('Home');
    } , [jwt]);
    const onLogin = () => dispatch(userLogin({email, password}));
    const onRegister = () => navigation.navigate('Register');

    return (
            <SafeAreaView style={style.container}>
                <View>
                    <Image style={style.image} source={Bg}/>
                    <TextInput style={style.input} onChangeText={setEmail} placeholder="Username"/>
                    <TextInput style={style.input} onChangeText={setPassword} placeholder="Password"/>   
                    <Button style={style.button} onPress={onLogin} title="Login" color="black"/>
                    <Text>Dont have an account ?</Text>
                    <Text style={style.intent} onPress={onRegister}>Register</Text>
                </View>
            </SafeAreaView>
    )
}

const style = StyleSheet.create ({
    container: {
        flex: 1,
        width: width,
        height: height,
        alignItems: "center",
    },
    view:{
        flexDirection: 'column',
        marginBottom: 20,
        textAlign: 'center'
    },
    input : {
        alignSelf: 'center',
        borderRadius: 20,
        margin: 5,
        backgroundColor: 'white',
        width: inp,
        marginBottom: 20
    },
    image:{
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain'
    },
    button:{
        paddingBottom: 20
    },
    intent:{
        color: 'black',
        fontWeight: 'bold'
    }
})

export default Login;