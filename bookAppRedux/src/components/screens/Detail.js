import { useNavigationState } from "@react-navigation/native";
import React, {useState, useEffect} from "react";
import {Share, TouchableHighlight, RefreshControl, StyleSheet, Dimensions, ScrollView, View, Image, Text} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getBookDetail } from "../../data/mutation";
import Icons from "react-native-vector-icons/Ionicons";
import Warning from "../modul/Warning";

const width = Dimensions.get('window').width; 
const height = Dimensions.get('window').height;
const wait = (timeout) => { 
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}



const DetailHome = ({navigation}) => {
    const id = useNavigationState(state => state.routes[state.index].params.id);
    const bookDetail = useSelector(state => state.bookDetail);
    const [image, setImage] = React.useState();

    const dispatch = useDispatch();
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        dispatch(getBookDetail({id: id}));
    }, [])

    const onRefresh = React.useCallback(() => {
        wait(2000).then(() => setRefreshing(false));
    }, [refreshing]);

    const onShare = async () => {
        try {
            const result = await Share.share({
                message: `${bookDetail.cover_image} - ${bookDetail.title} - ${bookDetail.author}`,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    <Warning/>
                } else {
                    alert("Shared")
                }
            } else if (result.action === Share.dismissedAction) {
                alert("Dismissed")
            }
        } catch (error) {
            alert(error.message);
        }
    }

    return (
       <View style={style.container}>
            <ScrollView showsHorizontalScrollIndicator={false} 
            refreshControl={
            <RefreshControl refreshing={refreshing}
             onRefresh={onRefresh} />
        }>
            {bookDetail && (
            <View>
                <Image style={style.bgImage} source={{uri: bookDetail.cover_image}}/>
                <View style={style.card}>
                    <TouchableHighlight>
                        <View>
                            <Icons name="arrow-redo-circle-outline" size={30} color="black" style={style.back} onPress={onShare}>
                                <Text>Share</Text>
                            </Icons>

                        </View>
                    </TouchableHighlight>
                    <View style={style.text}>
                        <Text style={{color:"black",fontSize: 24, fontFamily: "Open Sans", fontWeight: "bold"}}>{bookDetail.title}</Text>
                        <Text style={{fontSize: 12, color:"black"}}>{bookDetail.author}</Text>
                        <Text style={{color:"black"}}>{bookDetail.page_count}</Text>
                        <Text style={{color:"black"}}>{bookDetail.publisher}</Text>
                    </View>
                </View>
                <View style={style.content}>
                    <Text style={style.titleText}>Synopsis</Text>
                    <Text>{bookDetail.synopsis}</Text>     
                </View>
                <View style={style.content}>
                    <Text style={style.titleText}>Price</Text>
                    <Text>{bookDetail.price}</Text>
                </View>
            </View>
            )}     
            </ScrollView>
        </View>
    );

}

const style = StyleSheet.create({
    container:{
        flex: 1,
        width: width,
        height: height
    },
    content:{
        flex: 1,
        paddingBottom: 30,
        
        
    },
    titleText:{
        color: "black",
        fontSize: 18,
        fontWeight: "bold",
        marginBottom: 5

    },
    card:{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        height: 189,
        marginTop: 66
    },
    text:{
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-start",
    },
    bgImage : {
        flex: 1,
        width: 117,
        height: 167,
    } 
});

export default DetailHome;