import {
    configureStore
} from "@reduxjs/toolkit";
import * as api from './api';


const initialState = {
    jwt: null,
    bookList: null,
    bookDetail: null,
};

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_LOGIN":
            return {
                ...state,
                jwt: action.payload,
            };
        case "CLEAR_LOGIN":
            return {
                ...state,
                jwt: null,
            };
        case "SET_BOOKLIST":
            return {
                ...state,
                bookList: action.payload,
            };
        case "SET_BOOKDETAIL":
            return {
                ...state,
                bookDetail: action.payload,
            };
        default:
            return state;
    }
}

const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware({
            thunk: {
                extraArgument: api
            }
        })
})


export default store