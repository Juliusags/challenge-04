import axios from "axios";

const loginUrl = "http://code.aldipee.com/api/v1/auth/login";
const registerUrl = "http://code.aldipee.com/api/v1/auth/register";
const bookListUrl = "http://code.aldipee.com/api/v1/books";
const bookDetailUrlPrefix = "http://code.aldipee.com/api/v1/books/";


export const login = async (dispatch, getState, data) => {
    const response = (await axios.post(loginUrl, data)).data;
    console.log(response)
    dispatch({type: 'SET_LOGIN', payload: response.tokens.access.token})
};

export const register = async (dispatch, getState, data) => {
    const response = (await axios.post(registerUrl, data)).data;
    console.log(response)
    dispatch({type: 'SET_LOGIN', payload: response.access_token})
};

export const logout = async (dispatch, getState, data) => {
    dispatch('CLEAR_LOGIN')
};

export const bookList = async (dispatch, getState, params) => {
    const response = (await axios.get(bookListUrl, {
        params,
        headers: {
            Authorization: `Bearer ${getState().jwt}`
        }
    })).data;
    console.log(response)
    dispatch({type: 'SET_BOOKLIST', payload: response.results})
};

export const bookDetail = async (dispatch, getState, data) => {
    console.log(bookDetailUrlPrefix + data.id)
    const response = (await axios.get(bookDetailUrlPrefix + data.id, {
        headers: {
            Authorization: `Bearer ${getState().jwt}`
        }
    })).data;
    console.log(response)
    dispatch({type: 'SET_BOOKDETAIL', payload: response})
};
