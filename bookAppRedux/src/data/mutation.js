/**
 * @param {{email: string, password: string}} data
 */
 export function userLogin(data) {
    return (dispatch, getState, api) => {
        handleError(api.login(dispatch, getState, data))
       
    }
}

/**
 * @param {{name: string, email: string, password: string}} data
 */
export function userRegister(data) {
    return (dispatch, getState, api) => {
        handleError(api.register(dispatch, getState, data))
    }
}

/**
 * @param {{limit: string?, title: string?}} data
 */
 export function getBookList(data) {
    return (dispatch, getState, api) => {
        handleError(api.bookList(dispatch, getState, data))
    }
}

/**
 * @param {{id: string}} data
 */
 export function getBookDetail(data) {
    return (dispatch, getState, api) => {
        handleError(api.bookDetail(dispatch, getState, data))
    }
}


function handleError(promise) {
    promise.catch(err => {
        console.log(err);
    });
}
