import React, {useEffect, useState} from "react";
import {
  Dimensions,
  StyleSheet,
  Text,
  Image,
  View,
  useColorScheme
} from "react-native";
import { Colors } from "react-native/Libraries/NewAppScreen";
import NetInfo from "@react-native-community/netinfo";
import MainNavigation from "./src/components/MainNavigation"
import { Provider } from "react-redux";
import store from "./src/data/store";

const { width, height } = Dimensions.get("window");

const App = () => {
  const isDarkMode = useColorScheme() === "dark";

  const backgroundColor = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter
  };

  NetInfo.fetch().then(state => {
    console.log("Connection type", state.type);
    console.log("Is connected?", state.isConnected);
});

  const[splash, setSplash] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setSplash(false);
    }, 2000);
  }, []);

  return splash ? (
    <View style={style.container}>
        <Image style={style.image} source={require('./src/assets/logo.png')}/>
        <Text style={style.text}>var nick name</Text>
    </View>) : (
      <Provider store={store}>
        <MainNavigation />
      </Provider>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
  },
  image: {
    flex: 1,
    resizeMode: "contain",
    alignSelf: "center",
  },
  titleText:{
    color: "black",
    fontSize: 24,
    fontWeight: "bold",
  }
})

export default App;